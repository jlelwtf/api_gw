import os
import aiohttp
import numpy as np
from fastapi import FastAPI

from cluster import Cluster

app = FastAPI()

EMBEDDER_URL = os.getenv('EMBEDDER_URL')
INDEX_URL = os.getenv('INDEX_URL')
CLUSTERS_CENTERS_PATH = os.getenv('CLUSTERS_CENTERS_PATH')
VERSION = int(os.getenv('VERSION'))
K = 10

cluster = Cluster(CLUSTERS_CENTERS_PATH)


@app.get("/version")
async def version():
    return {'version': VERSION}


@app.get("/health")
async def health():
    return {'status': 'ok'}


@app.get("/get_answer")
async def read_item(question: str = 'test'):
    async with aiohttp.ClientSession() as session:
        async with session.post(EMBEDDER_URL, json={"inputs": [question]}) as resp:
            res = await resp.json()
    embedding = np.array(res['outputs'][0])
    cluster_num = cluster.find_cluster(embedding)

    index_query_json = {
        'k': K,
        'cluster': str(cluster_num),
        'embedding': embedding.tolist()
    }

    async with aiohttp.ClientSession() as session:
        async with session.post(INDEX_URL, json=index_query_json) as resp:
            res = await resp.json()

    return res
