import pickle

import numpy as np


class Cluster:
    def __init__(self, cluster_path: str):
        with open(cluster_path, 'rb') as f:
            clusters_centers = pickle.load(f)
        self.clusters_centers = np.array(list(clusters_centers.values()))

    def find_cluster(self, embedding: np.array):
        emb_norm = np.linalg.norm(embedding)
        centers_norm = np.linalg.norm(self.clusters_centers, axis=1)
        dot = np.dot(embedding, self.clusters_centers.T)
        res = dot / (emb_norm * centers_norm)
        return np.argmax(res)