FROM python:3.9

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

# install the dependencies and packages in the requirements file
RUN pip install -r requirements.txt

# copy every content from the local file to the image
COPY . /app
HEALTHCHECK --interval=10s --retries=2 CMD curl -f 'http://localhost:5000/health'

ENTRYPOINT ["uvicorn", "app:app", "--port", "5000", "--host", "0.0.0.0"]
